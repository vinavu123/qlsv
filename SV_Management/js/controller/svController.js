function layThongTinTuForm(){
    var ma = document.getElementById("txtMaSV").value
    var ten = document.getElementById("txtTenSV").value
    var email = document.getElementById("txtEmail").value
    var pass = document.getElementById("txtPass").value
    var toan = document.getElementById("txtDiemToan").value*1
    var ly = document.getElementById("txtDiemLy").value*1
    var hoa = document.getElementById("txtDiemHoa").value*1
    var sinhVien = new SinhVien(ma,ten,email,pass,toan,ly,hoa)
    return sinhVien
}
function renderSV(svArr){
    var contentHTML = ""
    for( var i = 0; i < svArr.length; i++){
        var sv = svArr[i]
        var content = `
        <tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>${sv.dtb()}</td>
        <td><button onclick="xoaSV(${sv.ma})" class="btn btn-danger">Xoa</button>
        <button onclick="suaSV(${sv.ma})" class="btn btn-primary" >Sua</button></td>

        </tr>
    
        `
        contentHTML += content 
        document.getElementById("tbodySinhVien").innerHTML = contentHTML
    }
}
function showThongTinLenForm(sv) {
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.toan;
    document.getElementById("txtDiemLy").value = sv.ly;
    document.getElementById("txtDiemHoa").value = sv.hoa;
  }