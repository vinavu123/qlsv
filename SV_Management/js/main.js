var listSV = []
var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
    listSV.push(sv);
  }
  renderDSSV(listSV);
}   

 function themSinhVien(){
    var thongTin = layThongTinTuForm()
    listSV.push(thongTin)
    var dataJson = JSON.stringify(listSV);

    localStorage.setItem("DSSV_LOCAL", dataJson);
    renderSV(listSV)
 }
 function xoaSV(id) {
    var viTri = -1;
    for (var i = 0; i < listSV.length; i++) {
      var viTriSV = listSV[i];
      if (viTriSV.ma == id) {
        viTri = i;
      }
    }
  listSV.splice(viTri,1);
  renderSV(listSV);
}
function suaSV(id){
  var viTri = listSV.findIndex(function(item) {
    return item.ma == id;
  });
  if (viTri != -1) {
    document.getElementById("txtMaSV").disabled = true;
    showThongTinLenForm(listSV[viTri]);
  }
}
function capNhatSinhVien(){
  document.getElementById("txtMaSV").disabled = false;
  var thongTin = layThongTinTuForm();
  var viTri = listSV.findIndex(function (item) {
    return item.ma == thongTin.ma;
  });
  if (viTri !== -1) {
    listSV[viTri] = thongTin;
    renderSV(listSV);
    resetForm();
  }
}
function resetForm() {
  document.getElementById("formQLSV").reset();
  
}